# CSharpLibraryBase



## Description

This project contains the needed files to build a C# class library, have it automatically packed into a NuGet package and optionally published to [NuGet.org](https://www.nuget.org/).

## Initial setup

First we have to get things set up on GitLab:
- [Create an empty project on GitLab](https://gitlab.com/projects/new).
- Go to *Settings* → *CI/CD*.
- Expand *Variables*.
- Click on *Add variable*.
- Enter **DOTNET_VERSION** as the *Key*.
- Enter which [version of the .NET SDK](https://versionsof.net/core/) you want to use as the *Value*. If in doubt, enter **6.0**.
- Click on *Add variable* to save.

**(Optional)** If you want the package to be uploaded to [NuGet.org](https://www.nuget.org/):
- Click on *Add variable* again.
- Enter **NUGET_API_KEY** as the *Key*.
- Enter a [NuGet API key](https://www.nuget.org/account/apikeys) as the *Value*. Do **not** share it!
- Make **sure** to check both *Protect Variable* and *Mask Variable*.
- Click on *Add variable* to save.

Now we have to get the empty project set up:
- Clone it using Git.
- Create a `develop` branch from `main`.
- Copy the [files from this project's main branch](https://gitlab.com/nodontdoit/csharplibrarybase/-/archive/main/csharplibrarybase-main.zip) over to it except for `LICENSE`, `README` and optionally `Rename.sh` (read below).

**(Optional)** If you want to rename the solution and project using the included utility:
- Run the `Rename.sh` file.
- Enter the new name you want to use for your solution and project. The utility renames both `CSharpLibraryBase.sln`, `CSharpLibraryBase.csproj` and every instance of the word `CSharpLibraryBase` in those two files.

Finally:
- Commit and push the changes.

## When making a new release

- Using an IDE, open your project's solution and edit the NuGet properties of your project.
- Bump the `Version` up. If this is not done, the package may fail to upload to [NuGet.org](https://www.nuget.org/) if publishing was configured.
- Copy the changes from `develop` to `main`. This will trigger a build. If publishing was configured, the package will be automatically published at the end.
- On GitLab, go to *CI/CD* → *Pipelines* to view the progress.
- You're done!

## Notes

The included solution by default targets `.NET Framework 4.7.2`  and uses `C# 7.3`. This is done so to be compatible with [Godot](https://godotengine.org/), but feel free to change it to your liking.
