echo "Please enter the new name:"
read Name
sed --in-place "s/CSharpLibraryBase/${Name}/g" CSharpLibraryBase.sln
mv CSharpLibraryBase.sln ${Name}.sln
sed --in-place "s/CSharpLibraryBase/${Name}/g" CSharpLibraryBase.csproj
mv CSharpLibraryBase.csproj ${Name}.csproj
rm Rename.sh